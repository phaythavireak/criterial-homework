package com.example.homework02criteriaapi.service;


import com.example.homework02criteriaapi.entities.Account;
import com.example.homework02criteriaapi.entities.Department;
import com.example.homework02criteriaapi.entities.Employee;
import com.example.homework02criteriaapi.entities.QEmployee;
import com.querydsl.core.QueryFactory;
import com.querydsl.core.types.EntityPath;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import javax.persistence.criteria.*;

import java.util.List;

@Service
@Transactional
public class EmployeeService implements QueryFactory {


    @PersistenceContext
    private EntityManager entityManager;


    private Logger logger = LogManager.getLogger();






    public List<Employee> number1()
    {
        //Already
        //criteria Query
        CriteriaBuilder criteriaBuilder=entityManager.getCriteriaBuilder();
        CriteriaQuery<Employee> criteriaQuery=criteriaBuilder.createQuery(Employee.class);
        Root<Employee> root=criteriaQuery.from(Employee.class);
        Join<Employee, Department> employees=root.join("department");
        criteriaQuery.select(root).orderBy(criteriaBuilder.desc(employees.get("id")));
        Query query=entityManager.createQuery(criteriaQuery);
        List<Employee> employee=query.getResultList();
        return employee;
    }
    public List number2(int id)
    {
        //Already
        CriteriaBuilder criteriaBuilder=entityManager.getCriteriaBuilder();
        CriteriaQuery<Employee> criteriaQuery=criteriaBuilder.createQuery(Employee.class);
        Root<Employee> root=criteriaQuery.from(Employee.class);
        Join<Employee, Department> employees=root.join("department");
        criteriaQuery.select(root).where(criteriaBuilder.equal(employees.get("id"),id));
        Query query=entityManager.createQuery(criteriaQuery);
        List<Employee> employee=query.getResultList();
        return employee;


    }
    public List<Employee> number3(int id)
    {
        //Already
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Employee> criteriaQuery = criteriaBuilder.createQuery(Employee.class);
        Root<Employee> employeeRoot = criteriaQuery.from(Employee.class);
        criteriaQuery.where(criteriaBuilder.equal(employeeRoot.get("id"),id));
        Query query = entityManager.createQuery(criteriaQuery);
        List<Employee> employees = query.getResultList();
        return employees;
    }
    public List<Tuple> number4()
    {
        //Already
        CriteriaBuilder criteriaBuilder=entityManager.getCriteriaBuilder();
        CriteriaQuery<Tuple> criteriaQuery=criteriaBuilder.createTupleQuery();
        Root<Employee> root=criteriaQuery.from(Employee.class);
        Join<Employee, Department> employees=root.join("department");
        criteriaQuery.select(criteriaBuilder.tuple(criteriaBuilder.sum(root.get("salary"))));
        criteriaQuery.groupBy(employees);
        Query query=entityManager.createQuery(criteriaQuery);
        List<Tuple> departments=query.getResultList();
//        for (Tuple tuple : departments) {
//           // System.out.println("salary = " + tuple.get(0) + " ");
//            tuple.get(0);
//        }
        return departments;

    }
    public List<Tuple> number5()
    {
        //Already
        CriteriaBuilder criteriaBuilder=entityManager.getCriteriaBuilder();
        CriteriaQuery<Tuple> criteriaQuery=criteriaBuilder.createTupleQuery();
        Root<Employee> root=criteriaQuery.from(Employee.class);
        criteriaQuery.select(criteriaBuilder.tuple(root.get("id"),root.get("empName"),root.get("gender"),root.get("salary")));
        Query query=entityManager.createQuery(criteriaQuery);
        List<Tuple> departments=query.getResultList();
        for (Tuple tuple : departments) {
            System.out.println("First name = " + tuple.get(0) + " "
                    + "Last Name = " + tuple.get(1)
                    + "Gender = " + tuple.get(2)
                    + "salary = " + tuple.get(3)
            );
        }
        return departments;

    }
    public List<Department> number6(int id)
    {
        // Already
        CriteriaBuilder criteriaBuilder=entityManager.getCriteriaBuilder();
        CriteriaQuery<Department> criteriaQuery=criteriaBuilder.createQuery(Department.class);
        Root<Department> root=criteriaQuery.from(Department.class);
        Join<Department,Employee> department=root.join("employees");
        criteriaQuery.select(root).where(criteriaBuilder.equal(department.get("id"),id));
        Query query=entityManager.createQuery(criteriaQuery);
        List<Department> departments=query.getResultList();
        return departments;

    }
    public List<Employee> number7()
    {
        //Already
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Employee> criteriaQuery = criteriaBuilder.createQuery(Employee.class);
        Root employeeRoot = criteriaQuery.from(Employee.class);
        criteriaQuery.select(criteriaBuilder.max(employeeRoot.get("salary")));
        Query query = entityManager.createQuery(criteriaQuery);
        List<Employee> employees = query.getResultList();
        return employees;
    }
    public List<Employee> number8()
    {
        //Already
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Employee> criteriaQuery = criteriaBuilder.createQuery(Employee.class);
        Root employeeRoot = criteriaQuery.from(Employee.class);
        criteriaQuery.select(employeeRoot).where(criteriaBuilder.between(employeeRoot.get("salary"),100,500));
        Query query = entityManager.createQuery(criteriaQuery);
        List<Employee> employees = query.getResultList();
        return employees;
    }
    public boolean number9()
    {
        //Already
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaUpdate update = criteriaBuilder.createCriteriaUpdate(Employee.class);
        Root employee = update.from(Employee.class);
        update.set("salary", 900);
        update.where(criteriaBuilder.equal(employee.get("department").get("id"), 2));
        Query query = entityManager.createQuery(update);
        query.executeUpdate();
        return true;
    }
    public boolean number10delete(int id)
    {
        //Already
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaDelete delete = criteriaBuilder.createCriteriaDelete(Employee.class);
        Root account = delete.from(Account.class);
        delete.where(criteriaBuilder.equal(account.get("employee").get("id"), id));
        int query = entityManager.createQuery(delete).executeUpdate();
        return true;

    }
    public boolean number10(int id){

        number10delete(id);
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaDelete delete = criteriaBuilder.createCriteriaDelete(Employee.class);
        Root employee = delete.from(Employee.class);
        delete.where(criteriaBuilder.equal(employee.get("id"), id));
        int query = entityManager.createQuery(delete).executeUpdate();
        return true;
    }
    public boolean number10update()
    {

        //Already
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaUpdate update = criteriaBuilder.createCriteriaUpdate(Employee.class);
        Root employee = update.from(Employee.class);
        update.set("empName", "Sdach game");
        update.where(criteriaBuilder.equal(employee.get("id"), 1));
        Query query = entityManager.createQuery(update);
        query.executeUpdate();
        return true;

    }

    @Override
    public com.querydsl.core.Query<?> query() {
        return null;
    }
}
