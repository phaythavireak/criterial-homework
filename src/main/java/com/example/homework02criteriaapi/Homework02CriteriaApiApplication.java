package com.example.homework02criteriaapi;

import com.example.homework02criteriaapi.entities.Account;
import com.example.homework02criteriaapi.entities.Address;
import com.example.homework02criteriaapi.entities.Department;
import com.example.homework02criteriaapi.entities.Employee;
import com.example.homework02criteriaapi.enums.Gender;
import com.example.homework02criteriaapi.reporsitories.EmployeeReporsitories;
import com.example.homework02criteriaapi.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Tuple;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Collections;
import java.util.List;

@SpringBootApplication
@Transactional
public class Homework02CriteriaApiApplication implements ApplicationRunner {


    public Homework02CriteriaApiApplication(EmployeeReporsitories employeeReporsitories, EmployeeService employeeService) {
        this.employeeReporsitories = employeeReporsitories;
        this.employeeService = employeeService;
    }

    @PersistenceContext
    private EntityManager entityManager;
    private EmployeeReporsitories employeeReporsitories;
    private EmployeeService employeeService;

    public static void main(String[] args) {
        SpringApplication.run(Homework02CriteriaApiApplication.class, args);
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {


        Address address = new Address("109","24","BTB","Cambodia");



        Department department = new Department();
        Account account = new Account();
        Department department1 = new Department();
        Account account1 = new Account();
        Department department2 = new Department();
        Account account2 = new Account();
        Department department3 = new Department();
        Account account3 = new Account();




        Employee employee = new Employee("virak",Gender.male,address,19.0,department,account);
        Employee employee1 = new Employee("thon",Gender.male,address,500.0,department,account);
        Employee employee2 = new Employee("pheaktra",Gender.male,address,200.0,department2,account2);
        Employee employee3 = new Employee("kheang",Gender.male,address,1000.0,department3,account3);

        department.setDepartmentName("IT");
        department.setEmployees(Collections.singletonList(employee));
        department1.setDepartmentName("ACC");
        department1.setEmployees(Collections.singletonList(employee1));
        department2.setDepartmentName("DES");
        department2.setEmployees(Collections.singletonList(employee2));
        department3.setDepartmentName("ARC");
        department3.setEmployees(Collections.singletonList(employee3));


        account.setAccountNumber("101010");
        account.setAmount(10.5);
        account.setEmployee(employee);
        account1.setAccountNumber("202020");
        account1.setAmount(20.5);
        account1.setEmployee(employee1);
        account2.setAccountNumber("303030");
        account2.setAmount(30.5);
        account2.setEmployee(employee2);
        account3.setAccountNumber("404040");
        account3.setAmount(40.5);
        account3.setEmployee(employee3);




        employeeReporsitories.save(employee);
        employeeReporsitories.save(employee1);
        employeeReporsitories.save(employee2);
        employeeReporsitories.save(employee3);

        System.out.println(employeeService.number1());







    }
}
