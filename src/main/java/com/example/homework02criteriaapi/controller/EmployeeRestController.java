package com.example.homework02criteriaapi.controller;


import com.example.homework02criteriaapi.entities.Department;
import com.example.homework02criteriaapi.entities.Employee;
import com.example.homework02criteriaapi.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.persistence.Tuple;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("api/v1")
public class EmployeeRestController {

    @Autowired
    private EmployeeService employeeService;


    @GetMapping("/number1")
    public Map<String,Object> getall()
    {
        Map<String,Object> map = new HashMap<>();
        List<Employee> employee = employeeService.number1();
        if (employee != null)
        {
            map.put("status",true);
            map.put("message","Data Found");
            map.put("data",employee);
        }else
        {
            map.put("status",false);
            map.put("message","Data not Found");
        }
        return map;
    }
    @GetMapping("/number2/{id}")
    public Map<String,Object> number2(@PathVariable Integer id)
    {
        Map<String,Object> map = new HashMap<>();
        List<Employee> employee = employeeService.number2(id);
        if (employee != null)
        {
            map.put("status",true);
            map.put("message","Data Found");
            map.put("data",employee);
        }else
        {
            map.put("status",false);
            map.put("message","Data not Found");
        }
        return map;
    }
    @GetMapping("/number3/{id}")
    public Map<String,Object> number3(@PathVariable Integer id)
    {
        Map<String,Object> map = new HashMap<>();
        List<Employee> employee = employeeService.number3(id);
        if (employee != null)
        {
            map.put("status",true);
            map.put("message","Data Found");
            map.put("data",employee);
        }else
        {
            map.put("status",false);
            map.put("message","Data not Found");
        }
        return map;
    }
    @GetMapping("/number4")
    public Map<String,Object> number4()
    {
        Map<String,Object> map = new HashMap<>();
       List<Tuple> employee = employeeService.number4();
        List<Object> employees = new ArrayList<>();
        for (Tuple t:
             employee) {
            employees.add("Salary ="+t.get(0));
        }
        if ( employees != null)
        {
            map.put("status",true);
            map.put("message","Data Found");
            map.put("data",employees);
        }else
        {
            map.put("status",false);
            map.put("message","Data not Found");
        }
        return map;
    }
    @GetMapping("/number5")
    public Map<String,Object> number5()
    {
        Map<String,Object> map = new HashMap<>();
        List<Tuple> employee = employeeService.number5();
        List<Object> employees = new ArrayList<>();
        for (Tuple t:
                employee) {
            employees.add("ID ="+t.get(0));
            employees.add("Employee ="+t.get(1));
            employees.add("Gender ="+t.get(2));
            employees.add("Salary ="+t.get(3));
            employees.add("----------------------");
        }
        if ( employees != null)
        {
            map.put("status",true);
            map.put("message","Data Found");
            map.put("data",employees);
        }else
        {
            map.put("status",false);
            map.put("message","Data not Found");
        }
        return map;
    }
    @GetMapping("/number6/{id}")
    public Map<String,Object> number6(@PathVariable Integer id)
    {
        Map<String,Object> map = new HashMap<>();
        List<Department> departments = employeeService.number6(id);
        if (departments != null)
        {
            map.put("status",true);
            map.put("message","Data Found");
            map.put("data",departments);
        }else
        {
            map.put("status",false);
            map.put("message","Data not Found");
        }
        return map;
    }
    @GetMapping("/number7")
    public Map<String,Object> number7()
    {
        Map<String,Object> map = new HashMap<>();
        List<Employee> employees = employeeService.number7();
        if (employees != null)
        {
            map.put("status",true);
            map.put("message","Data Found");
            map.put("data",employees);
        }else
        {
            map.put("status",false);
            map.put("message","Data not Found");
        }
        return map;
    }
    @GetMapping("/number8")
    public Map<String,Object> number8()
    {
        Map<String,Object> map = new HashMap<>();
        List<Employee> employees = employeeService.number8();
        if (employees != null)
        {
            map.put("status",true);
            map.put("message","Data Found");
            map.put("data",employees);
        }else
        {
            map.put("status",false);
            map.put("message","Data not Found");
        }
        return map;
    }
    @PutMapping("/number9")
    public Map<String,Object> number9()
    {
        Map<String,Object> map = new HashMap<>();

        if (employeeService.number9() != false)
        {
            map.put("status",true);
            map.put("message","Data Found");
        }else
        {
            map.put("status",false);
            map.put("message","Data not Found");
        }
        return map;
    }
    @DeleteMapping("/number10delete/{id}")
    public Map<String,Object> number10delete(@PathVariable int id)
    {
        Map<String,Object> map = new HashMap<>();
        if (employeeService.number10delete(id) != false)
        {
            map.put("status",true);
            map.put("message","Data Found");
        }else
        {
            map.put("status",false);
            map.put("message","Data not Found");
        }
        return map;
    }
    @PutMapping("/number10update")
    public Map<String,Object> number()
    {
        Map<String,Object> map = new HashMap<>();
        employeeService.number10update();
        if (employeeService.number10update() != false)
        {
            map.put("status",true);
            map.put("message","Data Found");
        }else
        {
            map.put("status",false);
            map.put("message","Data not Found");
        }
        return map;
    }
}
