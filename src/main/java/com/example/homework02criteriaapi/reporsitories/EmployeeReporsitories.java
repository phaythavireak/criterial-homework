package com.example.homework02criteriaapi.reporsitories;


import com.example.homework02criteriaapi.entities.Department;
import com.example.homework02criteriaapi.entities.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface EmployeeReporsitories extends JpaRepository<Employee,Long> {

}
